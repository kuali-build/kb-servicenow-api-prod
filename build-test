#!/bin/bash

set -euo pipefail

PROJECT_NAME="servicenow"
TIMESTAMP=$(date +"%Y-%m-%d-%H-%M-%S")
BASE_DIR="/srv/persistent-data/app/$PROJECT_NAME"
NEWDIR="$BASE_DIR/new/$TIMESTAMP"
PROD_DIR="/srv/persistent-data/app/servicenow/servicenow"

echo "Hostname: $(hostname --fqdn)"
echo "Working directory: $(pwd)"
sudo mkdir -p "$NEWDIR"
sudo chown -R gitlab-runner:gitlab-runner "$NEWDIR"

cp -R * "$NEWDIR/"

cd "$NEWDIR" || exit
declare -A services=(
    ["$PROJECT_NAME"]="Dockerfile"
)

for service in "${!services[@]}"; do
    DOCKERFILE="${services[$service]}"
    TAG_NAME="${service}:$TIMESTAMP"

    if [ -f "$DOCKERFILE" ]; then
        echo "Building $service"
        sudo docker build --no-cache --file "$DOCKERFILE" --tag "$service:latest" "$NEWDIR"
        sudo docker tag "$service:latest" "$TAG_NAME"

        TIMESTAMP_FILE="$BASE_DIR/build-timestamp-$service"
        sudo touch "$TIMESTAMP_FILE"
        echo "$TIMESTAMP" | sudo tee "$TIMESTAMP_FILE" > /dev/null

        echo "Successfully built and tagged $service with tag $TAG_NAME"
    else
        echo "Dockerfile for $service does not exist, skipping..."
    fi
done

# This is so strange...
echo "Copying contents to $PROD_DIR"
sudo rsync -av --delete "$NEWDIR/" "$PROD_DIR/"
sudo chown -R gitlab-runner:gitlab-runner "$PROD_DIR"

echo "Running production script"
cd "$PROD_DIR" || exit
./run-production

echo "All services have been processed and the production script has been executed."
