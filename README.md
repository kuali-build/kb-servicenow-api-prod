# Kuali Build: Create a ServiceNow Task

[![pipeline status](https://gitlab.oit.duke.edu/kuali-build/kb-servicenow-api-service-prod/badges/trunk/pipeline.svg)](https://gitlab.oit.duke.edu/kuali-build/kb-servicenow-api-service-prod/-/commits/trunk)

This project aims to provide a seamless integration between the Kuali Build form and the Duke Service Now API. It allows for creation of tasks between Kuali Build forms and the service now instance.  This process has proven to be tricky to figure out but you can review the ServiceNow api documentation here:  https://docs.servicenow.com/en-US/bundle/utah-api-reference

For this process, we primarily wanted to focus on `sn_sc`; which can be found here: https://docs.servicenow.com/en-US/bundle/utah-api-reference/page/integrate/inbound-rest/concept/c_ServiceCatalogAPI.html#title_servicecat-POST-items-submit_prod
## Endpoints Overview...

1. This endpoint retrieves servicenow user<br>
Example Requests:<br>
```bash
curl -X 'GET' \
  'https://kuali-build-api.oit.duke.edu:8092/{netid}' \
  -H 'accept: application/json' \
  -u 'username:password'

Example response:
{
    "full_name": "string",
    "first_name": "string",
    "last_name": "string",
    "dukeid": "string",
    "netid": "string",
    "email": "string",
    "username": "string",
    "servicenow_user_id": "string",
    "phone": "string",
    "title": "string",
    "department_name": "string",
    "cost_center_number": "string",
    "cost_center_name": "string"
}
```

2. This endpoint retrieves servicenow user manager<br>
Example Requests:<br>
```bash
curl -X 'GET' \
  'https://kuali-build-api.oit.duke.edu:8092/{netid}/manager' \
  -H 'accept: application/json' \
  -u 'username:password' 

Example response:
{
    "full_name": "string",
    "first_name": "string",
    "last_name": "string",
    "dukeid": "string",
    "netid": "string",
    "email": "string",
    "username": "string",
    "servicenow_user_id": "string",
    "phone": "string",
    "title": "string",
    "department_name": "string",
    "cost_center_number": "string",
    "cost_center_name": "string"
}
```

3. This endpoint retrieves a unique list of accepted `assignment_group`'s <br>
Example Requests:<br>
```bash
curl -X 'GET' \
  'https://kuali-build-api.oit.duke.edu:8092/assignment_groups' \
  -H 'accept: application/json' \
  -u 'username:password' 

QUERY PARAMETERS
user_select - string (User Select)
form - string (Form)
aka - string (Aka)
description - string (Description)

Example response:
  {
    "User select": "Audio Visual-Classroom Support-TTS-A&S",
    "sys_id": "44290e70919ca000f03e3891d38c4468",
    "description": "Arts&Sciences-TTS-AVClassroom",
    "aka": "A/V and Classroom",
    "form": "trinity_help_request"
  },
```

```

4. This endpoint retrieves a unique list of accepted `locations`'s <br>
Example Requests:<br>
```bash
curl -X 'GET' \
  'https://kuali-build-api.oit.duke.edu:8092/locations' \
  -H 'accept: application/json' \
  -u 'username:password' 

QUERY PARAMETERS - None

Example response:
  {
    "name": "100 Duke Health Cary Pl",
    "sys_id": "2a7858941bb9855064da98e8b04bcb5d",
    "full_name": "100 Duke Health Cary Pl"
  }
```


5. Create the task or incident in Service Now.<br>
Example Requests:<br>
```bash
curl -X 'POST' \
  'https://kuali-build-api.oit.duke.edu:8092/create-task' \
  -H 'accept: application/json' \
  -u 'username:password' 

QUERY PARAMETERS
string (Form Name) - Default: "Kuali Build Platform"
string (Category) - Default: "service_request"
string (Subcategory) - Default: "add"
integer (Priority) - Default: 3

DATA PARAMETERS
request (Request) - From Kuali Build Workflow Integration
```

## Project Architecture

This project contains instructions for 2 containers - 

1. web-kb-service-now-api-service-prod
2. caddy-kb-service-now-api-service-prod

## Project Structure

The main project file is `endpoints.py` which contains our general framework/setup, middleware, routes and requested business logic. It uses the FastAPI framework to create the APIs, and Pydantic library for data validation.

## Installation<br>

Before you can run this project, you'll need to install the dependencies.<br>
Docker engine:
```sh
sudo apt-get update \
    && sudo apt-get install docker.io docker-doc docker-compose podman-docker containerd runc
```
## Docker Compose<br>

** Please note:  kb-service-now-api-service-prod is the project name as named for this project directory in GitLab<br>

1. The `web` service:
    - The Docker image is built from the current directory, tagged as `kuali-kb-service-now-api-service-prod`.<br>
    - The container is named `web-kb-service-now-api-service-prod`.<br>
    - The container exposes port 7092 to ahe host at port 0.0.0.0:8092.<br>
    - Two volumes are mounted to the container. The first volume mounts the `.env` configuration file from the host to the `/app/.env` path in the container. The second volume mounts the log directory from the host to the `/app/log/` path in the container.<br>
    - The container will be restarted automatically unless it is explicitly stopped.<br>

2. in production an *nginx* proxy is used to terminate https.
In development The `caddy` service is used - but it would be a far, far better thing to run nginx like we do in production and reduce the overall complexity.

## Environment variables<br>

```ruby
- SERVICE_NOW_PROD_INSTANCE='DUKE'
- SVC_ACCT_USERNAME='xxx_service_account_username_xxx'
- SVC_ACCT_PASSWORD='xxx_service_account_password_xxx'
- BASIC_AUTH_USERNAME='xxx_create_username_xxx'
- BASIC_AUTH_PASSWORD='xxx_create_password_xxx'
```

## Installation<br>

1. Clone the repository to your server or linux/unix-based system.
2. Navigate to the project directory in a terminal.
3. Build the Docker image using the following command:

```bash
docker-compose build
```

## Usage

- To test the application in a Docker container, use the following command:

```bash
docker-compose up
```

- To shut down this test, use the following command:

```bash
docker-compose down
```

- To run the application in the background, use the following command:

```bash
docker-compose up -d
```
