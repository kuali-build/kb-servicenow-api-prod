from typing import Any

from loguru import logger

from configurations import ENV
import models as mdl
from models import TaskData
from query import kuali_post_response, servicenow_response


def format_description(task: mdl.ServiceNow):
    if task.meta and task.meta.form_container and task.meta.submitted_by:
        description_all = (
            f"Kuali Build Form: {task.meta.form_container.label}\n"
            f"Form Submission #: {task.meta.serial_number}\n"
            f"Submitted by name: {task.meta.submitted_by.display_name}\n"
            f"Submitted by email: {task.meta.submitted_by.email}"
        )
        work_notes = (
            f"{task.meta.submitted_by.display_name} "
            "has just submitted a request via the "
            f"{task.meta.form_container.label} form "
            f"located within Kuali Build.\n\n{description_all}"
        )
        return work_notes, task.meta.submitted_by.email
    return None, None


def return_target_users(task: mdl.ServiceNow):
    return (
        task.myself
        if task.request_is_for
        and task.request_is_for.label
        and task.request_is_for.label.casefold() == "myself"
        else task.someone_else
    )


def return_user_sys_id(target):
    data = target.id if target else None
    logger.debug(data)
    return data


def return_sys_id(task: mdl.ServiceNow):
    return (
        task.assignment_group.id
        if task and task.assignment_group and task.assignment_group.data
        else None
    )


def return_kuali_form_name(task: mdl.ServiceNow):
    form = (
        f"{task.meta.form_container.label} form"
        if task and task.meta and task.meta.form_container and task.meta.submitted_by
        else None
    )
    default_short_description = (
        f"{form} submitted by {task.meta.submitted_by.display_name} ({task.meta.submitted_by.school_id})"
        if task and task.meta and task.meta.form_container and task.meta.submitted_by
        else None
    )
    return form, default_short_description


async def RetrieveTaskData(
    task: mdl.ServiceNow, instance, request_type, x_response_url=None
):
    work_notes, email = format_description(task)
    target = return_target_users(task)
    requested_for_id = return_user_sys_id(target)
    task.u_impact_phone = target.data.phone if target and target.data else None

    form, default_short_description = return_kuali_form_name(task)
    sys_id = return_sys_id(task)
    short_description = task.short_description or default_short_description

    data = TaskData(
        assigned_to=task.assigned_to,
        u_vendor_reference=task.u_vendor_reference,
        assignment_group=sys_id,
        u_creation_group=sys_id,
        u_primary_group=sys_id,
        u_it_service=task.it_service,
        u_application=task.application,
        service_offering=task.service_offering,
        request_item=short_description,
        short_description=short_description,
        description=task.descriptions,
        u_sensitive=task.u_sensitive,
        contact_type=task.contact_type,
        u_location=form,
        active=task.active,
        comments=task.comments,
        work_notes=work_notes,
        u_sc_generic_requested_for=requested_for_id,
        caller_id=requested_for_id,
        category=task.category,
        impact=task.impact,
        state=task.state,
        priority=task.priority,
        urgency=task.urgency,
        u_service_provider=task.u_service_provider,
        u_requested_for=requested_for_id,
        u_category=task.category,
        u_subcategory=task.subcategory,
        subcategory=task.subcategory,
        company=task.company,
        u_impact_phone=task.u_impact_phone,
        watch_list=email,
        location=task.location,
        request_item_u_requested_for=requested_for_id,
    )

    logger.debug(data.model_dump_json(exclude_none=True))

    url = ENV.incident_url if request_type == "incident" else ENV.task_url
    response = await (
        create_sn_incident if request_type == "incident" else create_sn_task
    )(data.model_dump(exclude_none=True), url.format(instance.casefold()))

    kuali_data = {"task": response.model_dump(), "user_email": email}
    if x_response_url and response:
        kuali_post = await kuali_post_response(
            x_response_url, kuali_data, str(response.status_code)
        )
        return kuali_data | kuali_post or {}
    return kuali_data


async def _create_resource(data: dict, url: str, result_model: Any, log_message: str):
    reply, status_code = await servicenow_response(url, data, result_model)
    if reply and reply.result:
        logger.success(log_message.format(reply.result.number, reply.result.sys_id))
        return {
            "number": reply.result.number,
            "sys_id": reply.result.sys_id,
            "status_code": status_code,
        }


async def create_sn_task(data: dict, url: str):
    data = {"variables": data}
    response = await _create_resource(
        data,
        url,
        mdl.TaskResults,
        "Task created successfully with task_id: {}, status: {}",
    )
    return mdl.KualiResponse.model_validate(response)


async def create_sn_incident(data: dict, url: str):
    response = await _create_resource(
        data,
        url,
        mdl.IncidentResults,
        "Incident created successfully with incident_id: {}, status: {}",
    )
    return mdl.KualiResponse.model_validate(response)
