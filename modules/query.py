import asyncio
from typing import Any
from typing import Dict, Optional

import httpx
from httpx import Response
from loguru import logger
from pydantic import ConfigDict, Field, create_model
from stamina import retry

from configurations import ENV
import models as mdl


header_values = ENV.httpx_headers.model_dump()
exluded_headers = {"X-Status-Code": ENV.httpx_headers.x_status_code}
header_values_exclude_status = ENV.httpx_headers.model_dump(exclude=exluded_headers)



def create_dynamic_response_model(response: Dict[str, Any]):
    config = ConfigDict(str_strip_whitespace=True, ser_json_bytes='utf8', ser_json_timedelta='iso8601', str_to_upper=True, from_attributes=True)
    def create_fields(data: Dict[str, Any]):
        fields = {}
        for key, value in data.items():
            if isinstance(value, dict):
                nested_model = create_dynamic_response_model(value)
                if nested_model is not None:
                    fields[key] = (Optional[nested_model], Field(None))
            else:
                fields[key] = (Optional[str], Field(None))
        return fields

    if not response:
        return None

    fields = create_fields(response)
    return create_model('ResponseModel', __config__=config, **fields)

def process_response(response: dict):
    def replace_empty_strings(data: Dict[str, Any]) -> Dict[str, Any]:
        for key, value in data.items():
            if isinstance(value, dict):
                data[key] = replace_empty_strings(value)
            elif value == "":
                data[key] = None
        return data

    response = replace_empty_strings(response)
    if ResponseModel := create_dynamic_response_model(response):
        return ResponseModel.model_validate(response, from_attributes=True)


async def fetch_link_data(link):
    return await async_stream_query("GET", link, {})


async def should_create_task(key, value):
    query_iter: set = {"cost_center", "manager", "department", "company", "parent"}
    return (
        key in query_iter
        and isinstance(value, dict)
        and "link" in value
        and "value" in value
        and len(value["value"]) == 32
    )


async def query_service_now(q: mdl.QueryServiceNow):
    url = q.url()
    params = q.params()
    response_json = await async_stream_query("GET", url, params)
    if response_json:
        results = response_json.get("result", [])
        if not results:
            logger.warning("No results returned from ServiceNow.")
            return None

        async with asyncio.TaskGroup() as tg:
            tasks = []
            for result in results:
                for key, value in result.items():
                    if await should_create_task(key, value):
                        task = tg.create_task(fetch_link_data(value["link"]))
                        tasks.append((task, result, key))

            for task, result, key in tasks:
                link_data = await task
                result[key] = link_data

        if results:
            return results[0] if q.result_type.value.casefold() == "one" else results
        raise IndexError("No valid results after processing.")
    raise ValueError("No valid results after processing.")


@retry(on=(httpx.HTTPStatusError, httpx.ConnectTimeout))
async def handle_response_errors(response: Response):
    response.raise_for_status()
    logger.debug(response.url)
    if response.is_error:
        logger.exception(response.status_code)
        logger.debug(response.text)
        raise


async def async_stream_query(method: str, url: str, params: dict):
    async with httpx.AsyncClient(
        auth=get_servicenow_auth(url),
        timeout=30.0,
        headers=header_values_exclude_status,
    ) as client:
        async with client.stream(method, url, params=params) as response:
            await response.aread()
            await handle_response_errors(response)
            return response.json()


async def servicenow_response(url: str, data: dict, model: Any):
    async with httpx.AsyncClient(
        auth=get_servicenow_auth(url),
        timeout=30.0,
        headers=header_values_exclude_status,
    ) as client:
        response: Response = await client.post(url, json=data)
        if test := process_response(response.json()):
            logger.debug(test.model_dump_json(exclude_none=True, indent=2, round_trip=True))
        await handle_response_errors(response)
        return model.model_validate(response.json()), response.status_code


async def kuali_post_response(url: str, data: dict, status_code: str):
    ENV.httpx_headers.x_status_code = status_code
    async with httpx.AsyncClient(
        timeout=30.0,
        headers=ENV.httpx_headers.model_dump(),
    ) as client:
        response: Response = await client.post(url, json=data)
        await handle_response_errors(response)
        return response.json()


def get_servicenow_auth(url):
    return httpx.BasicAuth(
        ENV.username.get_secret_value(),
        (
            ENV.sandbox_password.get_secret_value()
            if "dukesandbox" in url
            else ENV.production_password.get_secret_value()
        ),
    )
