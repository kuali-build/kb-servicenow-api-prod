from __future__ import annotations

from pathlib import Path
from loguru import logger
from pydantic import (
    ConfigDict,
    EmailStr,
    Field,
    SecretStr,
    FilePath,
    DirectoryPath,
    field_validator,
    BaseModel,
)
from pydantic_settings import BaseSettings, SettingsConfigDict
from httpx_auth import Basic
from fastapi import Header


class HeaderValues(BaseModel):
    model_config = ConfigDict(coerce_numbers_to_str=True)
    content_type: str = Header(default="application/json")
    accept: str = Header(default="application/json")
    x_status_code: str = Header(default="201")


class ProductionInstance(BaseSettings):
    url: str = "https://kuali-build-api.oit.duke.edu:8092"
    description: str = "Production"


class DevelopmentInstance(BaseModel):
    url: str = "https://kuali-build-api-sjc-01.oit.duke.edu/servicenow"
    description: str = "Development"


class ProjectContact(BaseSettings):
    url: str = "https://oit.duke.edu"
    name: str = "Duke Office of Information Technology"
    email: EmailStr = "help@duke.edu"


class EnvironmentConfig(BaseSettings):
    model_config = SettingsConfigDict(
        str_strip_whitespace=True,
        frozen=True,
        env_file="/srv/persistent-data/app/servicenow/config/env",
        env_file_encoding="utf-8",
        extra="ignore",
    )

    # App credentials
    username: SecretStr = Field(validation_alias="SVC_ACCT_USERNAME")
    sandbox_password: SecretStr = Field(validation_alias="SANDBOX_PASSWORD")
    production_password: SecretStr = Field(validation_alias="PRODUCTION_PASSWORD")
    basic_auth_username: SecretStr = Field(validation_alias="BASIC_AUTH_USERNAME")
    basic_auth_password: SecretStr = Field(validation_alias="BASIC_AUTH_PASSWORD")

    # ServiceNow URLs
    base_url: str = Field(validation_alias="BASE_URL")
    table_url: str = Field(validation_alias="TABLE_URL")
    task_url: str = Field(validation_alias="TASK_URL")
    incident_url: str = Field(validation_alias="INCIDENT_URL")

    # General App Configuration
    app_title: str = "Kuali Build ServiceNow Integrations"
    app_version: str = "0.2.1"
    app_description: str = (
        "Used to integrate servicenow endpoints into Kuali Build."
        "  Allows users to create tasks, incidents, and query"
    )
    app_auth_headers: dict = {"WWW-Authenticate": "Basic"}
    exclude_headers: list = ["authorization", "cookie"]
    unauthorized_attempt: str = "Unauthorized attempt on '{}' by {}"
    production_server: ProductionInstance = ProductionInstance()
    development_server: DevelopmentInstance = DevelopmentInstance()
    contact: ProjectContact = ProjectContact()
    httpx_headers: HeaderValues = HeaderValues()

    # Directories
    parent_directory: DirectoryPath = Path(__file__).resolve().parent

    # Files
    log_file: FilePath | Path = parent_directory / "log" / "kb-servicenow-api-prod.log"


    @field_validator("log_file", mode="before")
    def check_log_file_exists(cls, v, values):
        if v is None:
            path = values.get("log_file")
            path.parent.mkdir(parents=True, exist_ok=True)
            path.touch(exist_ok=True)
            return path
        return v

    def servicenow_test_auth(self):
        return Basic(
            username=self.username.get_secret_value(),
            password=self.sandbox_password.get_secret_value(),
        )

    def servicenow_prod_auth(self):
        return Basic(
            username=self.username.get_secret_value(),
            password=self.production_password.get_secret_value(),
        )


ENV = EnvironmentConfig()


logger.add(
    ENV.log_file,
    level="INFO",
    enqueue=True,
    diagnose=True,
    backtrace=True,
    catch=True,
)
