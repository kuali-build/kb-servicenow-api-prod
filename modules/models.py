from __future__ import annotations
from enum import auto
import re
from typing import Annotated, Any, List, Optional, Union

from pydantic import (
    AwareDatetime,
    BaseModel,
    ConfigDict,
    EmailStr,
    Field,
    StringConstraints,
    validator,
)
from strenum import LowercaseStrEnum, SnakeCaseStrEnum, UppercaseStrEnum

from configurations import ENV

str_none_fields = Annotated[str | None, Field(default=None)]
int_none_fields = Annotated[int | None, Field(default=None)]


class ParentModel(BaseModel):
    model_config = ConfigDict(
        extra="ignore",
        ser_json_timedelta="iso8601",
        ser_json_bytes="utf8",
        str_strip_whitespace=True,
    )


class KualiResponse(BaseModel):
    number: str | None = None
    sys_id: str | None = None
    status_code: int


class KualiResponseAggr(BaseModel):
    task: KualiResponse | None = None
    user_email: EmailStr | None = None
    status_code: int | str | None = None


class Logs(BaseModel):
    wrong_format_returned: Any = "Something is oddly formatted: {}"


class EnumerableOptions(SnakeCaseStrEnum):
    CMDB_CI_APPL = auto()
    TASK = auto()
    INCIDENT = auto()
    CORE_COMPANY = auto()
    SC_REQ_ITEM = auto()
    SC_CAT_ITEM = auto()
    SC_CATEGORY = auto()
    SC_CATALOG = auto()
    CMDB_CI_BUSINESS_APP = auto()
    CMN_DEPARTMENT = auto()
    SERVICE_OFFERING = auto()
    U_ORGANIZATION = auto()
    CMN_LOCATION = auto()
    CMN_COST_CENTER = auto()
    CMDB_CI_SERVICE = auto()
    KB_KNOWLEDGE = auto()
    SYS_USER_GROUP = auto()
    SC_TASK = auto()
    CMDB_CI = auto()
    SYS_USER = auto()
    SYS_DB_OBJECT = auto()
    SYS_ATTACHMENT = auto()


class TaskOrIncident(LowercaseStrEnum):
    INCIDENT = auto()
    TASK = auto()


class Choices(SnakeCaseStrEnum):
    SYS_ID = auto()
    NUMBER = auto()
    NAME = auto()
    U_DUDUKEID = auto()
    DESCRIPTION = auto()
    UNIT_DESCRIPTION = auto()
    U_ENTITY = auto()
    USER_NAME = auto()
    U_DISPLAY_NAME = auto()
    U_EDUPERSONPRINCIPALNAME = auto()
    FILE_NAME = auto()
    GOTO123TEXTQUERY321 = auto()


class EnumerableSearchCharacteristics(UppercaseStrEnum):
    EQUALS = auto("=")
    NOT_EQUALS = auto("!=")
    IN = auto()
    NOT_IN = auto("NOT IN")
    LIKE = auto()
    STARTSWITH = auto()
    ENDSWITH = auto()
    GREATER_THAN = auto(">")
    LESS_THAN = auto("<")
    GREATER_THAN_OR_EQUAL = auto(">=")
    LESS_THAN_OR_EQUAL = auto("<=")
    AND = auto("^")
    OR = auto("OR")
    IS_EMPTY = auto("ISEMPTY")
    IS_NOT_EMPTY = auto("ISNOTEMPTY")
    BETWEEN = auto("BETWEEN")


class EnumerableInstances(LowercaseStrEnum):
    DUKE = auto()
    DUKESANDBOX = auto()


class TypeQuery(LowercaseStrEnum):
    ALL = auto()
    ONE = auto()


class QueryServiceNow(BaseModel):
    search_term: str
    extract_fields: Annotated[
        str | None, StringConstraints(strip_whitespace=True, to_lower=True)
    ] = None
    sysparm_limit: int = 50
    result_type: TypeQuery = TypeQuery.ALL
    search_table: EnumerableOptions
    search_type: EnumerableSearchCharacteristics
    search_by: Choices
    instance: EnumerableInstances = EnumerableInstances.DUKESANDBOX
    sysparm_filter_only: bool = True
    active: bool = True

    def construct_sysparm_query(self):
        return f"{self.search_by.value.lower()}{self.search_type.upper()}{self.search_term}"

    def url(self):
        return f"{ENV.table_url.format(ENV.base_url.format(self.instance.value.lower()))}{self.search_table.value.lower()}"

    def params(self):
        return {
            "active": self.active,
            "sysparm_filter_only": self.sysparm_filter_only,
            "sysparm_limit": self.sysparm_limit,
            "sysparm_query": self.construct_sysparm_query(),
            **({"sysparm_fields": self.extract_fields} if self.extract_fields else {}),
        }


class QueryServiceNowParams(BaseModel):
    sysparm_limit: int = 50
    sysparm_query: Annotated[
        str, StringConstraints(strip_whitespace=True, to_lower=True)
    ]
    sysparm_filter_only: bool = True
    active: bool = True
    search_type: Annotated[str, StringConstraints(strip_whitespace=True, to_upper=True)]
    search_by: Annotated[str, StringConstraints(strip_whitespace=True, to_lower=True)]
    search_term: Annotated[str, StringConstraints(strip_whitespace=True, to_lower=True)]
    sysparm_fields: Annotated[
        str, StringConstraints(strip_whitespace=True, to_lower=True)
    ]

    def construct_sysparm_query(self):
        return f"{self.search_by}{self.search_type}{self.search_term}"

    def construct_url(self):
        return f"{self.search_by}{self.search_type}{self.search_term}"

    def get_params(self):
        params = {
            "sysparm_limit": self.sysparm_limit,
            "sysparm_query": self.construct_sysparm_query(),
            "sysparm_filter_only": self.sysparm_filter_only,
            "active": self.active,
        }
        if self.sysparm_fields:
            params["sysparm_fields"] = self.sysparm_fields
        return params


class CreatedBy(ParentModel):
    id: str_none_fields
    label: str_none_fields
    display_name: str | None = Field(default=None, alias="displayName")
    username: str_none_fields
    email: EmailStr | None = None
    school_id: str | None = Field(default=None, alias="schoolId")
    first_name: str | None = Field(default=None, alias="firstName")
    last_name: str | None = Field(default=None, alias="lastName")


class SubmittedBy(ParentModel):
    id: str_none_fields
    label: str_none_fields
    display_name: str | None = Field(default=None, alias="displayName")
    username: str_none_fields
    email: EmailStr | None = None
    school_id: str | None = Field(default=None, alias="schoolId")
    first_name: str | None = Field(default=None, alias="firstName")
    last_name: str | None = Field(default=None, alias="lastName")


class FormContainer(ParentModel):
    id: str_none_fields
    label: str_none_fields


class Meta(ParentModel):
    created_by: CreatedBy | None = Field(default=None, alias="createdBy")
    submitted_by: SubmittedBy | None = Field(default=None, alias="submittedBy")
    created_at: int | None = Field(default=None, alias="createdAt")
    version_number: int | None = Field(default=None, alias="versionNumber")
    serial_number: str | None = Field(default=None, alias="serialNumber")
    app_publish_version: int | None = Field(default=None, alias="appPublishVersion")
    form_container: FormContainer | None = Field(default=None, alias="formContainer")
    workflow_total_runtime: None = Field(default=None, alias="workflowTotalRuntime")
    title: str_none_fields
    submitted_at: int | None = Field(default=None, alias="submittedAt")


class RequestIsFor(ParentModel):
    id: str_none_fields
    label: str_none_fields


class Data(ParentModel):
    cost_center_name: str_none_fields
    cost_center_number: str_none_fields
    department_name: str_none_fields
    dukeid: str_none_fields
    email: EmailStr | None = None
    first_name: str_none_fields
    full_name: str_none_fields
    last_name: str_none_fields
    netid: str_none_fields
    phone: str_none_fields
    servicenow_user_id: str_none_fields
    title: str_none_fields
    username: str_none_fields


class SomeoneElse(ParentModel):
    data: Data | None = None
    id: str_none_fields
    label: str_none_fields


class UserIsSelf(ParentModel):
    data: Data | None = None
    id: str_none_fields
    label: str_none_fields


class Datum(ParentModel):

    description: str_none_fields
    name: str_none_fields
    sys_id: str_none_fields


class AssignmentGroup(ParentModel):

    data: List[Datum] | Datum | None = None
    id: str_none_fields
    label: str_none_fields


class DescriptionField(ParentModel):
    id: str
    label: str


desc_items = Annotated[
    Optional[Union[str | float | int, DescriptionField]] | None, Field(default=None)
]


class ServiceNow(ParentModel):
    application_id: str | None = Field(default=None, alias="applicationId")
    document_id: str | None = Field(default=None, alias="documentId")
    form_id: str | None = Field(default=None, alias="formId")
    meta: Meta | None = None
    request_is_for: RequestIsFor | None = None
    user_phone: Annotated[str | None, Field(default=None, validation_alias="phone")]
    someone_else: SomeoneElse | None = None
    myself: UserIsSelf | None = Field(default=None, validation_alias="self")
    assignment_group: AssignmentGroup | None = None
    company: Annotated[str | None, Field(default="Duke University")]
    contact_type: Annotated[str | None, Field(default="self-service")]
    service_offering: str_none_fields
    short_description: str_none_fields
    location: str_none_fields
    description: str_none_fields
    priority: int_none_fields
    category: Annotated[str | None, Field(default="other")]
    subcategory: Annotated[str | None, Field(default="add")]
    urgency: Annotated[int | None, Field(default=2)]
    impact: int_none_fields
    u_impact_phone: Annotated[str | None, Field(default="phone")]
    u_service_provider: Annotated[
        str | None, Field(default="f63e4edd87f5c1009de73ece59434d87")
    ]
    comments: str_none_fields
    active: Annotated[bool, Field(default=True)]
    due_date: Annotated[AwareDatetime | None, Field(default=None)]
    state: Annotated[int | None, Field(default=1)]
    it_service: str_none_fields
    application: str_none_fields
    u_sensitive: Annotated[bool | None, Field(default=False)]
    assigned_to: str_none_fields
    u_vendor_reference: str_none_fields
    description1: desc_items = None
    description2: desc_items = None
    description3: desc_items = None
    description4: desc_items = None
    description5: desc_items = None
    description6: desc_items = None
    description7: desc_items = None
    description8: desc_items = None
    description9: desc_items = None
    description10: desc_items = None
    description11: desc_items = None
    description12: desc_items = None
    description13: desc_items = None
    description14: desc_items = None
    description15: desc_items = None
    description16: desc_items = None
    description17: desc_items = None
    description18: desc_items = None
    description19: desc_items = None
    description20: desc_items = None
    description21: desc_items = None
    description22: desc_items = None
    descriptions: str_none_fields

    @validator("descriptions", always=True, pre=True)
    def concatenate_descriptions(cls, v, values, **kwargs):
        description_keys = [key for key in values if re.match(r"description\d+$", key)]
        description_keys.sort(key=lambda x: int(x[len("description") :]))

        descriptions = []
        for key in description_keys:
            desc = values[key]
            if isinstance(desc, dict) and "label" in desc:
                descriptions.append(desc["label"])
            elif isinstance(desc, DescriptionField):
                descriptions.append(desc.label)
            elif isinstance(desc, int):
                if desc >= 10000:
                    money_value = desc / 100
                    formatted_money = f"${money_value:,.2f}"
                    descriptions.append(formatted_money)
                else:
                    descriptions.append(str(desc))
            elif desc is not None:
                descriptions.append(str(desc))

        if formatted_descriptions := [f"• {desc}\n" for desc in descriptions]:
            return "".join(formatted_descriptions)
        return values.get("description", "")


class TaskData(ParentModel):
    short_description: Optional[str]
    description: Optional[str]
    u_location: Optional[str]
    assignment_group: Optional[str]
    u_category: Optional[str]
    u_subcategory: Optional[str]
    u_sc_generic_requested_for: Optional[str]
    active: Optional[bool]
    u_sensitive: Optional[bool] = False
    contact_type: Optional[str]
    assigned_to: Optional[str] = None
    u_vendor_reference: Optional[str] = None
    impact: Optional[int]
    company: Optional[str]
    state: Optional[int]
    priority: Optional[int]
    urgency: Optional[int]
    u_service_provider: Optional[str]
    u_requested_for: Optional[str]
    u_impact_phone: Optional[str]
    request_item: Optional[str]
    request_item_u_requested_for: Optional[str]
    due_date: Optional[str] = None
    watch_list: Optional[str]
    u_creation_group: Optional[str]
    u_it_service: Optional[str]
    service_offering: Optional[str]
    u_application: Optional[str] = None
    work_notes: Optional[str]
    comments: Optional[str] = None
    u_primary_group: Optional[str]
    caller_id: Optional[str]
    category: Optional[str]
    subcategory: Optional[str]
    location: Optional[str]


class Variables(ParentModel):
    variables: TaskData


class ServiceNowResultItem(ParentModel):
    sys_id: str | None = None
    number: str | None = None
    parent_id: str | None = None
    record: str | None = None
    redirect_portal_url: str | None = None
    parent_table: str | None = None
    redirect_url: str | None = None
    table: str | None = None
    redirect_to: str | None = None


class TaskResults(ParentModel):
    result: ServiceNowResultItem | None = None


class IncidentUServiceProvider(ParentModel):
    link: str | None = None
    value: str | None = None


class IncidentUCallerDepartment(ParentModel):
    link: str | None = None
    value: str | None = None


class IncidentOpenedBy(ParentModel):
    link: str | None = None
    value: str | None = None


class IncidentSysDomain(ParentModel):
    link: str | None = None
    value: str | None = None


class IncidentCallerId(ParentModel):
    link: str | None = None
    value: str | None = None


class IncidentAssignmentGroup(ParentModel):
    link: str | None = None
    value: str | None = None


class IncidentCompany(ParentModel):
    link: str | None = None
    value: str | None = None


class IncidentResult(ParentModel):
    parent: str | None = None
    u_service_provider: IncidentUServiceProvider | str | None = None
    u_perl_api_http_user_agent: str | None = None
    caused_by: str | None = None
    watch_list: str | None = None
    u_dcri_trial: str | None = None
    upon_reject: str | None = None
    sys_updated_on: str | None = None
    origin_table: str | None = None
    approval_history: str | None = None
    u_sei_text: str | None = None
    skills: str | None = None
    number: str | None = None
    state: str | None = None
    u_ir_open_problems_details: str | None = None
    sys_created_by: str | None = None
    u_perl_api_http_referer: str | None = None
    knowledge: str | None = None
    order: str | None = None
    cmdb_ci: str | None = None
    contract: str | None = None
    impact: str | None = None
    active: str | None = None
    work_notes_list: str | None = None
    u_dcri_trial_related: str | None = None
    priority: str | None = None
    u_caused_by_undoc_chg: str | None = None
    u_total_service_users_impacted: str | None = None
    business_duration: str | None = None
    group_list: str | None = None
    u_ir_dependent_svcs_impacted: str | None = None
    u_template: str | None = None
    u_perl_api_user: str | None = None
    approval_set: str | None = None
    u_recovery_procedure_work_details: str | None = None
    universal_request: str | None = None
    short_description: str | None = None
    correlation_display: str | None = None
    work_start: str | None = None
    additional_assignee_list: str | None = None
    u_quantity: str | None = None
    u_end_date_time: str | None = None
    notify: str | None = None
    service_offering: str | None = None
    sys_class_name: str | None = None
    closed_by: str | None = None
    follow_up: str | None = None
    parent_incident: str | None = None
    reopened_by: str | None = None
    u_begin_date_time: str | None = None
    reassignment_count: str | None = None
    u_rec_proc_followed_details: str | None = None
    assigned_to: dict | str | None = None
    u_chat_queue_entry: str | None = None
    u_ir_it_status_url: str | None = None
    u_change_implemented: str | None = None
    u_ir_completed_on: str | None = None
    u_recovery_procedures_followed: str | None = None
    u_incident_review_status: str | None = None
    sla_due: str | None = None
    u_vendor_reference: str | None = None
    comments_and_work_notes: str | None = None
    u_user_communication: str | None = None
    u_ir_change_number: str | None = None
    u_caller_department: IncidentUCallerDepartment | str | None = None
    agile_story: str | None = None
    escalation: str | None = None
    u_ir_recovery_procedures: str | None = None
    upon_approval: str | None = None
    u_business_impact_description: str | None = None
    correlation_id: str | None = None
    u_location: str | None = None
    asset: str | None = None
    u_ir_problems: str | None = None
    made_sla: str | None = None
    u_perl_api_script_name: str | None = None
    u_change_resolved_incident: str | None = None
    u_major_incident: str | None = None
    u_converted_from: str | None = None
    u_perl_api_short_script_name: str | None = None
    u_taxonomy: str | None = None
    child_incidents: str | None = None
    task_effective_number: str | None = None
    resolved_by: str | None = None
    u_business_service: str | None = None
    u_ir_impact_type: str | None = None
    sys_updated_by: str | None = None
    u_resolution_notes: str | None = None
    u_vendor_hosted_support: str | None = None
    opened_by: IncidentOpenedBy | str | None = None
    user_input: str | None = None
    sys_created_on: str | None = None
    sys_domain: IncidentSysDomain | str | None = None
    u_impact_phone: str | None = None
    u_change_caused_incident: str | None = None
    u_ir_major_inc_declaration: str | None = None
    route_reason: str | None = None
    u_lessons_learned: str | None = None
    calendar_stc: str | None = None
    reassignment_count_individual: str | None = None
    u_ir_response: str | None = None
    closed_at: str | None = None
    u_contributing_factors: str | None = None
    u_message_type: str | None = None
    business_service: str | None = None
    business_impact: str | None = None
    rfc: str | None = None
    expected_start: str | None = None
    u_it_service: str | None = None
    opened_at: str | None = None
    u_ir_open_problems: str | None = None
    u_customer_segments: str | None = None
    work_end: str | None = None
    caller_id: IncidentCallerId | str | None = None
    reopened_time: str | None = None
    resolved_at: str | None = None
    u_phone_number: str | None = None
    subcategory: str | None = None
    work_notes: str | None = None
    u_sc_generic: str | None = None
    u_resolution_ci: str | None = None
    u_ir_problems_number: str | None = None
    u_could_we_have_prevented: str | None = None
    close_code: str | None = None
    u_full_description: str | None = None
    u_perl_api_ess_group: str | None = None
    assignment_group: IncidentAssignmentGroup | str | None = None
    u_major_incident_reviewer: str | None = None
    business_stc: str | None = None
    cause: str | None = None
    description: str | None = None
    u_perl_api_short_http_referer: str | None = None
    origin_id: str | None = None
    calendar_duration: str | None = None
    u_short_summary: str | None = None
    close_notes: str | None = None
    sys_id: str | None = None
    u_incident_created_change: str | None = None
    u_recovery_procedures_work: str | None = None
    contact_type: str | None = None
    u_converted_to: str | None = None
    u_sc_generic_converted: str | None = None
    u_change_implemented_number: str | None = None
    incident_state: str | None = None
    u_service_classification: str | None = None
    urgency: str | None = None
    problem_id: str | None = None
    u_entity_impacted: str | None = None
    u_perl_api_server_name: str | None = None
    company: IncidentCompany | str | None = None
    activity_due: str | None = None
    severity: str | None = None
    u_subject: str | None = None
    comments: str | None = None
    u_recovery_procedure_placed_details: str | None = None
    approval: str | None = None
    due_date: str | None = None
    sys_mod_count: str | None = None
    u_contributing_factors_description: str | None = None
    reopen_count: str | None = None
    u_ir_completed_by: str | None = None
    sys_tags: str | None = None
    u_entity_info: str | None = None
    u_recovery_procedures_followed_details: str | None = None
    u_caused_by_change: str | None = None
    u_creation_group: str | None = None
    u_application: str | dict | None = None
    u_is_change_documented: str | None = None
    u_perl_api_remote_addr: str | None = None
    u_hics_code_black: str | None = None
    u_close_code: str | None = None
    location: str | dict | None = None
    u_last_work_note: str | None = None
    category: str | None = None
    u_message: str | None = None
    u_inbound_email_to: str | None = None


class IncidentResults(ParentModel):
    result: IncidentResult | None = None


class TaskCreator(ParentModel):
    task_data: ServiceNow
    instance: EnumerableInstances = EnumerableInstances.DUKESANDBOX
    ticket_type: TaskOrIncident


class Results(ParentModel):
    number: str | None = None
    sys_id: str | None = None


class ReturnResults(ParentModel):
    task: Results | None = None
    user_email: str | None = None
