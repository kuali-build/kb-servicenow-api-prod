# External Libs
from fastapi import BackgroundTasks, Depends, Header, responses, status

from create import RetrieveTaskData
from middleware import initialize_app
from models import QueryServiceNow, TaskCreator
from query import query_service_now

app = initialize_app()


@app.get("/query", tags=["query records"])
async def get_items_from_servicenow(q: QueryServiceNow = Depends()):
    return await query_service_now(q)


@app.post("/create-task", tags=["create record"])
async def create_data(
    kb: BackgroundTasks,
    c: TaskCreator = Depends(),
    x_response_url: str | None = Header(None, alias="X-Response-URL"),
):
    if x_response_url:
        return responses.Response(
            status_code=status.HTTP_202_ACCEPTED,
            content="ServiceNow request accepted and processing started",
            background=kb.add_task(
                RetrieveTaskData,
                c.task_data,
                c.instance.value,
                c.ticket_type.value,
                x_response_url,
            ),
        )
    return responses.ORJSONResponse(
        status_code=status.HTTP_201_CREATED,
        content=await RetrieveTaskData(
            c.task_data, c.instance.value, c.ticket_type.value
        ),
    )
