import asyncio
from secrets import compare_digest
import traceback
from typing import Annotated, Optional, Union

from Secweb import SecWeb
from fastapi import (
    Depends,
    FastAPI,
    Request,
    Response,
    exception_handlers,
    exceptions,
    responses,
    security,
    status,
)
from typing import Dict, Any
from fastapi.middleware import cors, gzip
from fastapi.routing import APIRoute
from loguru import logger
from pydantic import ConfigDict, Field, create_model
from configurations import ENV

correct_username = ENV.basic_auth_username.get_secret_value().encode("utf8")
correct_password = ENV.basic_auth_password.get_secret_value().encode("utf8")


async def startup_event():
    logger.success("app started")


async def shutdown_event():
    logger.warning("Shutdown initiated. Cleaning up tasks and resources.")
    tasks = [t for t in asyncio.all_tasks() if t is not asyncio.current_task()]
    [task.cancel() for task in tasks]
    await asyncio.gather(*tasks, return_exceptions=True)
    logger.success("goodbye.")


def access(
    credentials: Annotated[security.HTTPBasicCredentials, Depends(security.HTTPBasic())]
):
    current_username = credentials.username.encode("utf8")
    is_correct_username = compare_digest(current_username, correct_username)

    current_password = credentials.password.encode("utf8")
    is_correct_password = compare_digest(current_password, correct_password)

    if not (is_correct_username and is_correct_password):
        raise exceptions.HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Incorrect username or password",
            headers=ENV.app_auth_headers,
        )

    return credentials.username


def process_headers(headers: Dict[str, Any]):
    config = ConfigDict(
        str_strip_whitespace=True,
        str_to_upper=True,
        from_attributes=True,
        validate_return=False,
    )

    def infer_type(value: Any):
        type_mapping = {
            bool: Optional[bool],
            int: Optional[int],
            float: Optional[float],
            str: Optional[str],
        }
        if isinstance(value, dict):
            nested_model = create_header_model(value)
            return Optional[nested_model] if nested_model is not None else Optional[str]
        return type_mapping.get(type(value), Optional[str])


    def create_header_model(data: Dict[str, Any]):
        fields = {}
        for key, value in data.items():
            if key.lower() in ["cookie", "authorization"]:
                continue
            field_type = infer_type(value)
            fields[key] = (field_type, Field(None))
        return create_model('HeaderModel', __config__=config, **fields)

    def replace_empty_strings(data: Dict[str, Any]) -> Dict[str, Any]:
        for key, value in data.items():
            if isinstance(value, dict):
                data[key] = replace_empty_strings(value)
            elif value == "":
                data[key] = None
        return data

    headers = replace_empty_strings(headers)
    HeaderModel = create_header_model(headers)
    return HeaderModel.model_validate(headers, from_attributes=True)

@logger.catch
def log_request_response(query_string, path, status_code, headers):
    log_format = f"{query_string} | {path} | {status_code} \n {headers}"
    if status_code in range(200, 299):
        logger.success(log_format)
    elif status_code in range(400, 499):
        logger.error(log_format)
    elif status_code in range(500, 599):
        logger.critical(log_format)
        logger.critical(headers)
    else:
        logger.info(log_format)


async def middlewares_stuffs(request: Request, call_next):
    headers = request.headers
    if header_dump := process_headers(headers):
        headers = header_dump
    response = await call_next(request)
    log_request_response(
        request.query_params.multi_items(),
        request.url.path,
        response.status_code,
        headers,
    )
    return response


async def http_exceptions(
    request: Request, exc: exceptions.HTTPException
) -> Union[responses.JSONResponse, Response]:
    if exc.status_code == 403:
        client_ip = request.client.host if request.client else "Unknown client host"
        logger.warning(ENV.unauthorized_attempt.format(request.url.path, client_ip))
    return await exception_handlers.http_exception_handler(request, exc)


@logger.catch
async def unhandled_exception_handler(request: Request, exc: Exception):
    last_call = traceback.extract_tb(exc.__traceback__)[-1]
    response_detail = {
        "host": getattr(getattr(request, "client", None), "host", None),
        "port": getattr(getattr(request, "client", None), "port", None),
        "method": request.method,
        "query_params": request.query_params._dict,
        "error_type": type(exc).__name__,
        "error_message": str(exc),
        "file": last_call.filename,
        "line": last_call.lineno,
        "function": last_call.name,
        "url": (
            f"{request.url.path}?{request.query_params}"
            if request.query_params
            else request.url.path
        ),
    }
    logger.exception(response_detail)
    return responses.PlainTextResponse(
        str(response_detail), status_code=status.HTTP_500_INTERNAL_SERVER_ERROR
    )


async def validation_exceptions(
    request: Request, exc: exceptions.RequestValidationError
):
    body = await request.body()
    query_params = request.query_params._dict
    detail = {
        "errors": exc.errors(),
        "body": body.decode(),
        "query_params": query_params,
    }
    logger.error(detail)
    return await exception_handlers.request_validation_exception_handler(request, exc)


def endpoint_unique_id(route: APIRoute):
    return f"{route.tags[0]}-{route.name}"


def initialize_app():
    app = FastAPI(
        root_path="/servicenow",
        docs_url=None,
        redoc_url=None,
        servers=[
            ENV.production_server.model_dump(),
            ENV.development_server.model_dump(),
        ],
        contact=ENV.contact.model_dump(),
        redirect_slashes=True,
        root_path_in_servers=False,
        title=ENV.app_title,
        version=ENV.app_version,
        description=ENV.app_description,
        dependencies=[Depends(access)],
        default_response_class=responses.ORJSONResponse,
        on_startup=[startup_event],
        on_shutdown=[shutdown_event],
        generate_unique_id_function=endpoint_unique_id,
        debug=False,
    )

    # middleware
    app.middleware("http")(middlewares_stuffs)
    app.add_middleware(gzip.GZipMiddleware, minimum_size=1000)
    app.add_middleware(
        cors.CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    # exception handlers
    app.add_exception_handler(exceptions.RequestValidationError, validation_exceptions)
    app.add_exception_handler(exceptions.HTTPException, http_exceptions)
    app.add_exception_handler(Exception, unhandled_exception_handler)

    # Additional security
    SecWeb(app=app)

    return app
