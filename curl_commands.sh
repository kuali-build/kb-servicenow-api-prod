curl -X 'GET' \
    --max-time 5 \
    --silent \
    --show-error \
    --fail \
    --compressed \
    --retry-connrefused \
    --user "${BASIC_AUTH_USERNAME}:${BASIC_AUTH_PASSWORD}" \
    --header "accept: application/json" \
    --url-query active=true \
    --url-query sysparm_filter_only=true \
    --url-query instance="duke" \
    --url-query search_by="name" \
    --url-query search_type="IN" \
    --url-query search_table="sys_user_group" \
    --url-query result_type="all" \
    --url-query sysparm_limit=8 \
    --url-query extract_fields="name,sys_id" \
    --url-query search_term=" \
    Audio Visual-Classroom Support-TTS-A&S, \
  	Device Support-ePrint-Labs-Imaging-OIT, \
    Device Support-Documentary Studies-A&S, \
    Device Support-ScienceDrive-Nicholas-OIT, \
    Network Services-TTS-A&S, \
    Device Support-East-Central-Downtown-OIT, \
    Device Support-West-VIP-OIT, \
    Web Services-OIT" \
    --url "https://kuali-build-api-sjc-01.oit.duke.edu/servicenow/query" | jq

[
  {
    "sys_id": "44290e70919ca000f03e3891d38c4468",
    "name": "Audio Visual-Classroom Support-TTS-A&S"
  },
  {
    "sys_id": "4c290e70919ca000f03e3891d38c445b",
    "name": "Device Support-Documentary Studies-A&S"
  },
  {
    "sys_id": "c6ef4ac2838a7d107725f130feaad3ca",
    "name": "Device Support-East-Central-Downtown-OIT"
  },
  {
    "sys_id": "0f40da06838a7d107725f130feaad3de",
    "name": "Device Support-ePrint-Labs-Imaging-OIT"
  },
  {
    "sys_id": "66109206838a7d107725f130feaad311",
    "name": "Device Support-ScienceDrive-Nicholas-OIT"
  },
  {
    "sys_id": "88009ec2838a7d107725f130feaad38b",
    "name": "Device Support-West-VIP-OIT"
  },
  {
    "sys_id": "00290e70919ca000f03e3891d38c446d",
    "name": "Network Services-TTS-A&S"
  },
  {
    "sys_id": "3c294e70919ca000f03e3891d38c447e",
    "name": "Web Services-OIT"
  }
]

curl -X 'GET' \
    --max-time 5 \
    --silent \
    --show-error \
    --fail \
    --compressed \
    --retry-connrefused \
    --user "${BASIC_AUTH_USERNAME}:${BASIC_AUTH_PASSWORD}" \
    --header "accept: application/json" \
    --url-query active=true \
    --url-query sysparm_filter_only=true \
    --url-query instance="duke" \
    --url-query search_by="name" \
    --url-query search_type="LIKE" \
    --url-query search_table="sys_user" \
    --url-query result_type="all" \
    --url-query sysparm_limit=50 \
    --url-query extract_fields=" \
    name, \
    sys_id, \
    u_dudukeid, \
    user_name, \
    u_display_name" \
    --url-query search_term="styles crawford-jennings" \
    --url "https://kuali-build-api-sjc-01.oit.duke.edu/servicenow/query" | jq

[
  {
    "sys_id": "0ebd78961bf1dd9073d51f87b04bcb1a",
    "u_display_name": "Styles Crawford-Jennings (sjc98)",
    "user_name": "sjc98",
    "name": "Styles Crawford-Jennings",
    "u_dudukeid": "1262516"
  }
]


curl -X 'GET' \
    --max-time 5 \
    --silent \
    --show-error \
    --fail \
    --compressed \
    --retry-connrefused \
    --user "${BASIC_AUTH_USERNAME}:${BASIC_AUTH_PASSWORD}" \
    --header "accept: application/json" \
    --url-query active=true \
    --url-query sysparm_filter_only=true \
    --url-query instance="duke" \
    --url-query search_by="u_dudukeid" \
    --url-query search_type="=" \
    --url-query search_table="sys_user" \
    --url-query result_type="one" \
    --url-query sysparm_limit=1 \
    --url-query extract_fields=" \
    name, \
    sys_id, \
    u_dudukeid, \
    user_name, \
    u_display_name" \
    --url-query search_term="1262516" \
    --url "https://kuali-build-api-sjc-01.oit.duke.edu/servicenow/query" | jq

{
  "sys_id": "0ebd78961bf1dd9073d51f87b04bcb1a",
  "u_display_name": "Styles Crawford-Jennings (sjc98)",
  "user_name": "sjc98",
  "name": "Styles Crawford-Jennings",
  "u_dudukeid": "1262516"
}



curl --max-time 5 \
    --silent \
    --show-error \
    --fail \
    --compressed \
    --retry-connrefused \
    --user "${BASIC_AUTH_USERNAME}:${BASIC_AUTH_PASSWORD}" \
    --url-query instance=dukesandbox \
    --url-query ticket_type=task \
    --url "https://kuali-build-api-sjc-01.oit.duke.edu/servicenow/create-task" \
    --json '{"applicationId":"65e11211f9a011013a6397f3","documentId":"6636f23b41c8f825ec00fbde","formId":"6636f23b41c8f825ec00fbde","meta":{"createdBy":{"id":"634d6e7088b226e144874098","label":"Styles Crawford-Jennings","displayName":"Styles Crawford-Jennings","username":"sjc98@duke.edu","email":"styles.crawford-jennings@duke.edu","schoolId":"1262516","firstName":"Styles","lastName":"Crawford-Jennings"},"submittedBy":{"id":"634d6e7088b226e144874098","label":"Styles Crawford-Jennings","displayName":"Styles Crawford-Jennings","username":"sjc98@duke.edu","email":"styles.crawford-jennings@duke.edu","schoolId":"1262516","firstName":"Styles","lastName":"Crawford-Jennings"},"createdAt":1714876986978,"serialNumber":"0031","appPublishVersion":18,"versionNumber":1,"formContainer":{"id":"65e11211f9a011013a6397fb","label":"new computer request"},"workflowTotalRuntime":null,"title":"0031","versionTitle":"--","submittedAt":1714877016403},"assignment_group":{"data":{"name":"Device Support-FS Asset Mgmt-DHTS","sys_id":"d0290e70919ca000f03e3891d38c44b0"},"id":"d0290e70919ca000f03e3891d38c44b0","label":"Device Support-FS Asset Mgmt-DHTS"},"request_is_for":{"id":"gQd91v01c","label":"Myself"},"self":{"data":{"email":"styles.crawford-jennings@duke.edu","name":"Styles Crawford-Jennings","phone":"+1 919 684 2200","sys_id":"dd46d1051baa1190bd24ebd5604bcb32","title":"Automation Developer","u_display_name":"Styles Crawford-Jennings (sjc98)","u_dudukeid":"1262516","user_name":"sjc98"},"id":"dd46d1051baa1190bd24ebd5604bcb32","label":"Styles Crawford-Jennings (sjc98)"},"description1":10000,"description2":"123456","description3":{"id":"CDEL9CPjj0r","label":"Windows Operating System"},"description5":{"id":"xTXOthzB2","label":"Standard Windows Laptop PC"},"description7":{"id":"Hwq52YannQP","label":"No, I do not need a monitor"},"description8":{"id":"PevT-9H4-D7","label":"No, I do not need a keyboard"},"description9":{"id":"fTUuVAjWHPJ","label":"No, I do not need a mouse"},"description11":{"id":"vx7dmYXLON0","label":"Lenovo ThinkPad X1 Yoga - $1,761.00. 13th Gen Intel Core i7-1355U | 16GB RAM | 512GB Storage"},"assigned_to":"dd46d1051baa1190bd24ebd5604bcb32"}' | jq

{
  "task": {
    "number": "TASK7721372",
    "sys_id": "428546291b9a42d0ee977732dd4bcb8e",
    "status_code": 200
  },
  "user_email": "styles.crawford-jennings@duke.edu"
}

curl --max-time 5 \
    --silent \
    --show-error \
    --fail \
    --compressed \
    --retry-connrefused \
    --user "${BASIC_AUTH_USERNAME}:${BASIC_AUTH_PASSWORD}" \
    --url-query instance=dukesandbox \
    --url-query ticket_type=incident \
    --url "https://kuali-build-api-sjc-01.oit.duke.edu/servicenow/create-task" \
    --json '{"applicationId":"65e11211f9a011013a6397f3","documentId":"6636f23b41c8f825ec00fbde","formId":"6636f23b41c8f825ec00fbde","meta":{"createdBy":{"id":"634d6e7088b226e144874098","label":"Styles Crawford-Jennings","displayName":"Styles Crawford-Jennings","username":"sjc98@duke.edu","email":"styles.crawford-jennings@duke.edu","schoolId":"1262516","firstName":"Styles","lastName":"Crawford-Jennings"},"submittedBy":{"id":"634d6e7088b226e144874098","label":"Styles Crawford-Jennings","displayName":"Styles Crawford-Jennings","username":"sjc98@duke.edu","email":"styles.crawford-jennings@duke.edu","schoolId":"1262516","firstName":"Styles","lastName":"Crawford-Jennings"},"createdAt":1714876986978,"serialNumber":"0031","appPublishVersion":18,"versionNumber":1,"formContainer":{"id":"65e11211f9a011013a6397fb","label":"new computer request"},"workflowTotalRuntime":null,"title":"0031","versionTitle":"--","submittedAt":1714877016403},"assignment_group":{"data":{"name":"Device Support-FS Asset Mgmt-DHTS","sys_id":"d0290e70919ca000f03e3891d38c44b0"},"id":"d0290e70919ca000f03e3891d38c44b0","label":"Device Support-FS Asset Mgmt-DHTS"},"request_is_for":{"id":"gQd91v01c","label":"Myself"},"self":{"data":{"email":"styles.crawford-jennings@duke.edu","name":"Styles Crawford-Jennings","phone":"+1 919 684 2200","sys_id":"dd46d1051baa1190bd24ebd5604bcb32","title":"Automation Developer","u_display_name":"Styles Crawford-Jennings (sjc98)","u_dudukeid":"1262516","user_name":"sjc98"},"id":"dd46d1051baa1190bd24ebd5604bcb32","label":"Styles Crawford-Jennings (sjc98)"},"description1":10000,"description2":"123456","description3":{"id":"CDEL9CPjj0r","label":"Windows Operating System"},"description5":{"id":"xTXOthzB2","label":"Standard Windows Laptop PC"},"description7":{"id":"Hwq52YannQP","label":"No, I do not need a monitor"},"description8":{"id":"PevT-9H4-D7","label":"No, I do not need a keyboard"},"description9":{"id":"fTUuVAjWHPJ","label":"No, I do not need a mouse"},"description11":{"id":"vx7dmYXLON0","label":"Lenovo ThinkPad X1 Yoga - $1,761.00. 13th Gen Intel Core i7-1355U | 16GB RAM | 512GB Storage"},"assigned_to":"dd46d1051baa1190bd24ebd5604bcb32"}' | jq

{
  "task": {
    "number": "INC4448242",
    "sys_id": "d0a54e651b128e90745ea82fbd4bcb31",
    "status_code": 201
  },
  "user_email": "styles.crawford-jennings@duke.edu"
}