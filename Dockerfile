FROM python:3.11.8-slim

SHELL ["/bin/bash", "-c"]

ENV DEBIAN_FRONTEND=noninteractive
WORKDIR /app

COPY requirements.txt /tmp/requirements.txt

RUN apt-get update && apt-get install -y --no-install-recommends \
    build-essential \
    vim-tiny \
    curl \
    && pip install --no-cache-dir --upgrade pip setuptools wheel \
    && pip install --no-cache-dir -r /tmp/requirements.txt \
    && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false build-essential \
    && rm -rf /var/lib/apt/lists/*

COPY modules/ /app/

CMD fastapi run --port 7092 endpoints.py